#!/usr/bin/env python

""" Simple Python Web server """

import mimetypes
import os
import sys # Only needed to get command line arguments
from socket import *
from string import *

class Server():
    backlog = 0

    def __init__(self, port, webRoot):
        self.backlog = 256
        mimetypes.init()
        self.start(port, webRoot)

    def start(self, port, webRoot):
        # Bind to all interfaces and start listening
        sock = socket(AF_INET, SOCK_STREAM)
        try:
            sock.bind(("", port))
            sock.listen(self.backlog)
        except:
            print "Unable to start server on port %i" % port
            return
        
        # Process incoming connections
        try:
            while True:
                connection, address = sock.accept()
                handler = HttpConnection(connection, address, webRoot)
                connection.close()
        except KeyboardInterrupt:
            print "\nShutting down server"
        sock.close()

class HttpConnection():
    maxRequestSize = 0
    supportedExtensions = ['html', 'htm', 'js', 'jpg', 'jpeg', 'png', 'gif',
                           'tif', 'bmp']

    def __init__(self, connection, address, webRoot):
        self.maxRequestSize = 32768

        # Handle incoming connection in a separate process
        if os.fork() == 0:
            self.start(connection, address, webRoot)

    def start(self, connection, address, webRoot):
        print "Incoming connection from", address
        buf = connection.recv(self.maxRequestSize)
        self.handleHttpRequest(connection, buf, webRoot)
        connection.close()
        exit()

    def handleHttpRequest(self, connection, buf, webRoot):
        path = ""
        try:
            # Get request type and path
            request, path, dontcare = split(buf, " ", 2)
            if request.lower() != "get":
                raise ValueError
        except ValueError:
            self.respond(connection, 400, "Bad Request",
                         "Error: 400 Bad Request")
            return

        self.buildResponseFromPath(connection, path, webRoot)

    def respond(self, connection, code, reason, message):
        response = "HTTP/1.0 %i %s\r\n" % (code, reason)
        response += "Content-Type: text/html\r\n\r\n"
        response += "<html>\n<title>Error %i</title>\n" % code
        response += "<body>%s</body>\n<html>\n" % message
        connection.sendall(response)

    def buildResponseFromPath(self, connection, path, webRoot):
        # Convert URL and webRoot to path
        try:
            fullpath = self.buildPath(path, webRoot)
        except:
            # Catch all path errors, including bad URL, invalid path
            print "Access denied to relative path: %s" % path
            self.respond(connection, 403, "Forbidden", "Error: 403 Forbidden")
            return

        # Check if requested path is a directory
        if os.path.isdir(fullpath):
            self.buildResponseDirListing(connection, fullpath, path, webRoot)
            return

        # Only serve files with specified extensions
        dontcare, extension = os.path.splitext(fullpath)
        extension = extension[1:]
        if extension.lower() not in self.supportedExtensions:
            print "Requested file is forbidden to be served: %s" % fullpath
            self.respond(connection, 403, "Forbidden", "Error: 403 Forbidden")
            return

        # Send response
        try:
            contentType, dontcare = mimetypes.guess_type(fullpath)
            headers = "HTTP/1.0 200 OK\r\n"
            headers += "Content-Type: %s\r\n" % contentType
            headers += "\r\n"
            connection.sendall(headers)

            # Send file
            print "Requested file %s" % fullpath
            sendFile = open(fullpath, "r")
            try:
                while True:
                    buf = sendFile.read(4096)
                    if buf == "":
                        break
                    connection.sendall(buf)
            finally:
                sendFile.close()

        except:
            print "Error responding to HTTP request"
        
        return

    def buildPath(self, path, webRoot):
        # Replace escaped characters
        idx = -1
        while True:
            idx = path.find("%", idx+1)
            if idx < 0:
                break
            char = "%c" % int(path[idx+1:idx+3], 16)
            path = "%s%s%s" % (path[0:idx], char, path[idx+3:])

        # Build full path resolving symbolic links
        realPath = os.path.realpath("%s/%s" % (webRoot, path))

        # Validate file
        realWebRoot = os.path.realpath(webRoot)

        # File must be inside web root
        if realWebRoot != realPath[0:len(realWebRoot)]:
            print "Requested path is not in web root: %s" % realPath
            raise ValueError
        # File must exist and not be a directory
        if os.path.exists(realPath) != True:
            print "Requested path does not exist: %s" % realPath
            raise ValueError

        return realPath

    def buildResponseDirListing(self, connection, fullpath, path, webRoot):
        # Send header
        header = "HTTP/1.0 200 OK\r\n"
        header += "Content-Type: text/html\r\n\r\n"
        header += "<html>\n<title>Directory Listing</title>\n"
        header += "  <body>\n"
        connection.sendall(header)

        # Link to parent directory
        webRoot = os.path.realpath(webRoot)
        if len(webRoot) < len(fullpath):
            connection.sendall("    <b>dir</b></b><a href=\"%s\">.."
                               "</a><br>\n" % os.path.dirname(path))

        # Generate directory listing
        listing = sorted(os.listdir(fullpath))

        # Show directories first
        for item in listing:
            if os.path.isdir("%s/%s" % (fullpath, item)):
                relUrl = self.makeRelativeUrl(path, item)
                connection.sendall("<b>dir</b> <a href=\"%s\">%s</a><br>\n" %
                                   (relUrl, item))
        # Show files
        for item in listing:
            if os.path.isdir("%s/%s" % (fullpath, item)) != True:
                relUrl = self.makeRelativeUrl(path, item)
                connection.sendall("file <a href=\"%s\">%s</a><br>\n" %
                                   (relUrl, item))
        # Footer
        connection.sendall("  </body>\n<html>\n")

    def makeRelativeUrl(self, path, item):
        path = path[1:]
        if len(path) > 0:
            return "%s/%s" % (path, item)
        else:
            return item
                
# Parse arguments
def usage(progname):
    print "Usage: %s [listen port] [web root]" % progname

port = 0
webRoot = ""

if len(sys.argv) != 3:
    usage(sys.argv[0])
    exit(1)

try:
    port = int(sys.argv[1])
    if port < 1 or port > 65535:
        print "Invalid port: %i" % port
        raise ValueError
    webRoot = sys.argv[2]
    if os.path.exists(webRoot) != True :
        print "Invalid www root dir: %s" % webRoot
        raise ValueError
except ValueError:
    usage(sys.argv[0])
    exit(1)

# Start server
server = Server(port, webRoot)
